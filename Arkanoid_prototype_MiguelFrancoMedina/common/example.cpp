#include "stdafx.h"
#include "sys.h"
#include "core.h"
#include "font.h"
#include <sstream>

struct Ball {
	vec2   pos;
	vec2   vel;
	GLuint gfx;
	float  radius;
};

struct Player {
	vec2 pos;
	vec2 vel;
	vec2 size;
	GLuint gfx;
	int lives;
	bool bigSize;
	bool shoot;
	bool speed;
};

struct Block {
	vec2 pos;
	vec2 vel;
	vec2 size;
	GLuint gfx;
};

#define NUM_BALLS 1
#define MAX_BALL_SPEED 8.f
#define PLAYER_SPEED 4.f

Ball balls[NUM_BALLS];
Player player;
std::vector<Block> blocks(10);
std::vector<Ball> ballLives;
std::vector<Block> bullets;
std::vector<Block> bullet;

bool checkRectRect(const vec2& rectPos1, const vec2& rectSize1, const vec2& rectPos2, const vec2& rectSize2);
bool checkCircleRect(const vec2& circlePos, float circleRadius, const vec2& rectPos, const vec2& rectSize);

template<class T>
const T clamp(const T& val, const T& min, const T& max);

int Main(void) {
	FONT_Init();
	//OutputDebugString("Hola");

	// Load textures
	GLuint texbkg = CORE_LoadPNG("../data/background_brick.png", true);
	GLuint texsmallball = CORE_LoadPNG("../data/ballsoccer.png", false);
	GLuint texPlayer = CORE_LoadPNG("../data/player.png", false);
	GLuint texBlock = CORE_LoadPNG("../data/block.png", false);
	GLuint texBallLive = CORE_LoadPNG("../data/heart.png", false);
	GLuint texBullets = CORE_LoadPNG("../data/bullets.png", false);
	GLuint texBullet = CORE_LoadPNG("../data/bullet.png", false);

	// Init game state
	for (int i = 0; i < NUM_BALLS; i++) {
		balls[i].pos = vmake(30, SCR_HEIGHT);
		balls[i].vel = vmake(-5, -5);
		balls[i].radius = 16.f;
		balls[i].gfx = texsmallball;
	}

	// Init player
	player.pos = vmake(50, 200);
	player.vel = vmake(CORE_FRand(-MAX_BALL_SPEED, +MAX_BALL_SPEED), CORE_FRand(-MAX_BALL_SPEED, +MAX_BALL_SPEED));
	player.size = vmake(70, 20);
	player.gfx = texPlayer;
	player.lives = 2;
	player.bigSize = false;
	player.shoot = false;
	player.speed = false;

	// Init blocks
	for (int i = 0; i < blocks.size(); i++) {
		blocks[i].pos = vmake(CORE_FRand(0, SCR_WIDTH + 20), CORE_FRand(200, SCR_HEIGHT - 20));
		blocks[i].vel = vmake(-10, -10);
		blocks[i].size = vmake(30, 30);
		blocks[i].gfx = texBlock;
	}

	// Set up rendering
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT); // Sets up clipping
	glClearColor(0.0f, 0.1f, 0.3f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCR_WIDTH, 0.0, SCR_HEIGHT, 0.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	while (!SYS_GottaQuit()) {
		
		// Render clear
		glClear(GL_COLOR_BUFFER_BIT);

		// Render background
		for (int i = 0; i <= SCR_WIDTH / 128; i++)
			for (int j = 0; j <= SCR_HEIGHT / 128; j++)
				CORE_RenderCenteredSprite(vmake(i * 128.f + 64.f, j * 128.f + 64.f), vmake(128.f, 128.f), texbkg);

		// Render balls
		for (int i = 0; i < NUM_BALLS; i++)
			CORE_RenderCenteredSprite(balls[i].pos, vmake(balls[i].radius * 2.f, balls[i].radius * 2.f), balls[i].gfx);

		// Render player
		CORE_RenderSprite(player.pos, vmake(player.pos.x + player.size.x, player.pos.y + player.size.y), player.gfx);

		// Render blocks
		for (int i = 0; i < blocks.size(); i++)
			CORE_RenderCenteredSprite(blocks[i].pos, vmake(blocks[i].size.x, blocks[i].size.y), blocks[i].gfx);

		// Render ballLives
		for (int i = 0; i < ballLives.size(); i++)
			CORE_RenderCenteredSprite(ballLives[i].pos, vmake(ballLives[i].radius * 2.f, ballLives[i].radius * 2.f), ballLives[i].gfx);

		// Render bullets
		for (int i = 0; i < bullets.size(); i++)
			CORE_RenderCenteredSprite(bullets[i].pos, vmake(bullets[i].size.x, bullets[i].size.y), bullets[i].gfx);

		// Render bullets
		for (int i = 0; i < bullet.size(); i++)
			CORE_RenderCenteredSprite(bullet[i].pos, vmake(bullet[i].size.x, bullet[i].size.y), bullet[i].gfx);

		// Text
		std::stringstream ss;
		ss << "LIVES: " << player.lives;
		FONT_DrawString(vmake(15, SCR_HEIGHT - 30), ss.str().c_str());

		if (blocks.size() == 0)
			FONT_DrawString(vmake(SCR_WIDTH / 2 - 10, SCR_HEIGHT / 2), "YOU WIN!");

		if (player.lives == 0)
			FONT_DrawString(vmake(SCR_WIDTH / 2 - 10, SCR_HEIGHT / 2), "YOU LOSE!");

		if (player.shoot) 
			FONT_DrawString(vmake(15, SCR_HEIGHT - 60), "CAN SHOOT!");

		SYS_Show();

		// Move player
		if (SYS_KeyPressed(SYS_KEY_UP)) {
			player.pos = vmake(player.pos.x, player.pos.y + 7);
		}

		if (SYS_KeyPressed(SYS_KEY_DOWN)) {
			player.pos = vmake(player.pos.x, player.pos.y - 7);
		}

		if (SYS_KeyPressed(SYS_KEY_LEFT)) {
			player.pos = vmake(player.pos.x - 7, player.pos.y);
		}

		if (SYS_KeyPressed(SYS_KEY_RIGHT)) {
			player.pos = vmake(player.pos.x + 7, player.pos.y);
		}

		if (SYS_KeyPressed(SYS_KEY_SPACE) && player.shoot) {
			Block newBullet;
			newBullet.pos = player.pos;
			newBullet.vel = vmake(7, 7);
			newBullet.size = vmake(20, 20);
			newBullet.gfx = texBullet;
			bullet.push_back(newBullet);
		}

		// Run balls
		for (int i = 0; i < NUM_BALLS; i++) {
			vec2 oldpos = balls[i].pos;
			vec2 newpos = vadd(oldpos, balls[i].vel);

			bool collision = false;
			int colliding_block = -1;
			for (int j = 0; j < blocks.size(); j++) { // TODO: Cambiar por colision con ladrillos.
				if (checkCircleRect(balls[i].pos, balls[i].radius, blocks[j].pos, blocks[j].size)) {
					collision = true;
					colliding_block = j;
					break;
				}
			}

			if (checkCircleRect(balls[i].pos, balls[i].radius, player.pos, player.size)) {
				collision = true;
			}

			if (!collision)
				balls[i].pos = newpos;
			else {
				// Rebound
				if (colliding_block != -1) {
					blocks.erase(blocks.begin() + colliding_block);
				}
				else if (balls[i].pos.x <= player.pos.x + player.size.x / 2 - 10)
					balls[i].vel.x = -5.f;
				else if (balls[i].pos.x <= player.pos.x + player.size.x / 2 + 10 && balls[i].pos.x >= player.pos.x + player.size.x / 2 - 10)
					balls[i].vel.x *= 0.f;
				else if (balls[i].pos.x >= player.pos.x + player.size.x / 2 + 10)
					balls[i].vel.x = 5.f;

				balls[i].vel.y *= -1.f;
				balls[i].pos = vadd(oldpos, balls[i].vel);
			}

			// Rebound on margins
			if (balls[i].vel.x > 0.0) {
				if (balls[i].pos.x > SCR_WIDTH)
					balls[i].vel.x *= -1.0;
			} else {
				if (balls[i].pos.x < 0)
					balls[i].vel.x *= -1.0;
			}
			if (balls[i].vel.y > 0.0) {
				if (balls[i].pos.y > SCR_HEIGHT)
					balls[i].vel.y *= -1.0;
			} else {
				if (balls[i].pos.y < 0) {
					balls[i].vel.y *= -1.0;
					player.lives--;
				}
			}
		}

		// Move ballLives
		for (int i = 0; i < ballLives.size(); i++) {
			ballLives[i].pos.y--;

			if (ballLives[i].pos.y < 0) {
				ballLives.erase(ballLives.begin() + i);
				if (i > 0)
					i--;
			}

			if (checkCircleRect(ballLives[i].pos, ballLives[i].radius, player.pos, player.size)) { // TODO: se sale de rango si elimina el i = 0 i se queda -1.
				ballLives.erase(ballLives.begin() + i);
				if (i > -1)
					i--;
				player.lives++;
			}
		}

		// Move bullets
		for (int i = 0; i < bullets.size(); i++) {
			bullets[i].pos.y--;

			if (bullets[i].pos.y < 0) {
				bullets.erase(bullets.begin() + i);
				if (i > 0)
					i--;
			}

			if (checkRectRect(bullets[i].pos, bullets[i].size, player.pos, player.size)) {
				bullets.erase(bullets.begin() + i);
				if (i > -1)
					i--;
				player.shoot = true;
			}
		}

		// Move bullet
		for (int i = 0; i < bullet.size(); i++) {
			bullet[i].pos.y++;

			if (bullet[i].pos.y > SCR_HEIGHT) {
				bullet.erase(bullet.begin() + i);
				if (i > 0)
					i--;
			}

			for (int j = 0; j < blocks.size(); j++) {
				if (checkRectRect(bullet[i].pos, bullet[i].size, blocks[j].pos, blocks[j].size)) { // TODO: Fuera de rango.
					blocks.erase(blocks.begin() + j);
					if (j > -1)
						j--;
					bullet.erase(bullet.begin() + i);
					if (i > -1)
						i--; 
				}
			}
		}

		int ran = CORE_FRand(0, 500);
		if (ran == 7) {
			Ball newBall;
			newBall.pos = vmake(CORE_FRand(30, SCR_WIDTH - 30), SCR_HEIGHT);
			newBall.vel = vmake(-7, -7);
			newBall.radius = 16.f;
			newBall.gfx = texBallLive;
			ballLives.push_back(newBall);
		}

		int ranBull = CORE_FRand(0, 200);
		if (ranBull == 7) {
			Block newBull;
			newBull.pos = vmake(CORE_FRand(30, SCR_WIDTH - 30), SCR_HEIGHT);
			newBull.vel = vmake(-7, -7);
			newBull.size = vmake(30, 30);
			newBull.gfx = texBullets;
			bullets.push_back(newBull);
		}

		// Keep system running
		SYS_Pump();
		SYS_Sleep(17);
	}

	CORE_UnloadPNG(texbkg);
	CORE_UnloadPNG(texPlayer);
	CORE_UnloadPNG(texsmallball);
	CORE_UnloadPNG(texBallLive);
	CORE_UnloadPNG(texBullet);
	CORE_UnloadPNG(texBullets);
	FONT_End();

	return 0;
}

bool checkCircleRect(const vec2& circlePos, float circleRadius,
	const vec2& rectPos, const vec2& rectSize) {
	float closestX = clamp(circlePos.x, rectPos.x, rectPos.x + rectSize.x);
	float closestY = clamp(circlePos.y, rectPos.y, rectPos.y + rectSize.y);	float distanceX = circlePos.x - closestX;	float distanceY = circlePos.y - closestY;
	if ((distanceX * distanceX) + (distanceY * distanceY) < circleRadius * circleRadius)
		return true;
	else
		return false;
}

bool checkRectRect(const vec2& rectPos1, const vec2& rectSize1,
	const vec2& rectPos2, const vec2& rectSize2) {

	if ((max(rectPos1.x, rectPos2.x) < min(rectPos1.x + rectSize1.x, rectPos2.x + rectSize2.x)) &&
		(max(rectPos1.y, rectPos2.y) < min(rectPos1.y + rectSize1.y, rectPos2.y + rectSize2.y))) {
		return true;
	}
	else return false;
}

template<class T>
const T clamp(const T& val, const T& min, const T& max) {
	return max(min, min(max, val));
}

////-----------------------------------------------------------------------------
//struct Ball
//{
//  vec2   pos;
//  vec2   vel;
//  GLuint gfx;
//  float  radius;
//};
//#define NUM_BALLS 20
//Ball balls[NUM_BALLS];
//
//#define MAX_BALL_SPEED 8.f
//
////-----------------------------------------------------------------------------
//int Main(void)
//{
//  FONT_Init();
//
//  // Load textures
//  GLuint texbkg        = CORE_LoadPNG("../data/circle-bkg-128.png"   , true);
//  GLuint texlargeball  = CORE_LoadPNG("../data/ball128.png"          , false);
//  GLuint texsmallball  = CORE_LoadPNG("../data/tyrian_ball.png"      , false);
//
//  // Init game state
//  for (int i = 0; i < NUM_BALLS; i++)
//  {
//    balls[i].pos = vmake(CORE_FRand(0.0, SCR_WIDTH), CORE_FRand(0.0, SCR_HEIGHT));
//    balls[i].vel = vmake(CORE_FRand(-MAX_BALL_SPEED, +MAX_BALL_SPEED), CORE_FRand(-MAX_BALL_SPEED, +MAX_BALL_SPEED));
//    if (CORE_FRand(0.f, 1.f) < 0.10f)
//    {
//      balls[i].radius = 64.f;
//      balls[i].gfx = texlargeball;
//    }
//    else
//    {
//      balls[i].radius = 16.f;
//      balls[i].gfx = texsmallball;
//    }
//  }
//
//  // Set up rendering
//  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT); // Sets up clipping
//  glClearColor( 0.0f, 0.1f, 0.3f, 0.0f );
//  glMatrixMode(GL_PROJECTION);
//  glLoadIdentity();
//  glOrtho( 0.0, SCR_WIDTH, 0.0, SCR_HEIGHT, 0.0, 1.0);
//  glEnable(GL_TEXTURE_2D);
//  glEnable(GL_BLEND);
//  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//  while (!SYS_GottaQuit())
//  {
//    // Render
//    glClear( GL_COLOR_BUFFER_BIT );
//
//    // Render backgground
//    for (int i = 0; i <= SCR_WIDTH/128; i++)
//      for (int j = 0; j <= SCR_HEIGHT/128; j++)
//        CORE_RenderCenteredSprite(vmake(i * 128.f + 64.f, j * 128.f + 64.f), vmake(128.f, 128.f), texbkg);
//
//    // Render balls
//    for (int i = 0; i < NUM_BALLS; i++)
//      CORE_RenderCenteredSprite(balls[i].pos, vmake(balls[i].radius * 2.f, balls[i].radius * 2.f), balls[i].gfx);
//
//    // Text
//    FONT_DrawString(vmake(SCR_WIDTH/2 - 6*16, 16), "HELLO WORLD!");
//   
//    SYS_Show();
//
//    // Run balls
//    for (int i = 0; i < NUM_BALLS; i++)
//    {
//      vec2 oldpos = balls[i].pos;
//      vec2 newpos = vadd(oldpos, balls[i].vel);
//
//      bool collision = false;
//      int colliding_ball = -1;
//      for (int j = 0; j < NUM_BALLS; j++)
//      {
//        if (i != j)
//        {
//          float limit2 = (balls[i].radius + balls[j].radius) * (balls[i].radius + balls[j].radius);
//          if (vlen2(vsub(oldpos, balls[j].pos)) > limit2 && vlen2(vsub(newpos, balls[j].pos)) <= limit2)
//          {
//            collision = true;
//            colliding_ball = j;
//            break;
//          }
//        }
//      }
//
//      if (!collision)
//        balls[i].pos = newpos;
//      else
//      {
//        // Rebound!
//        balls[i].vel = vscale(balls[i].vel, -1.f);
//        balls[colliding_ball].vel = vscale(balls[colliding_ball].vel, -1.f);
//      }
//
//      // Rebound on margins
//      if (balls[i].vel.x > 0.0)
//      {
//        if (balls[i].pos.x > SCR_WIDTH)
//          balls[i].vel.x *= -1.0;
//      } else {
//        if (balls[i].pos.x < 0)
//          balls[i].vel.x *= -1.0;
//      }
//      if (balls[i].vel.y > 0.0)
//      {
//        if (balls[i].pos.y > SCR_HEIGHT)
//          balls[i].vel.y *= -1.0;
//      } else {
//        if (balls[i].pos.y < 0)
//          balls[i].vel.y *= -1.0;
//      }
//    }
//   
//    // Keep system running
//    SYS_Pump();
//    SYS_Sleep(17);
//  }
//
//  CORE_UnloadPNG(texbkg);
//  CORE_UnloadPNG(texlargeball);
//  CORE_UnloadPNG(texsmallball);
//  FONT_End();
//
//  return 0;
//}
