#pragma once
#include "stdafx.h"
#include "applicationModeGame.h"
#include "applicationGestor.h"
#include "font.h"
#include "LocationManager.h"
#include <sstream>

using namespace rapidjson;

vec2   backPos;
vec2   backSize;
GLuint backTex;

ApplicationModeGame::ApplicationModeGame(int idLevel) {
	mIdLevel	   = idLevel;
	inputManager   = new InputManager();
	game		   = new Game();
	graphicsEngine = new GraphicsEngine();
	mPauseMode	   = new ApplicationModePauseMenu();
}

ApplicationModeGame::~ApplicationModeGame() {
	delete mPauseMode;
}

void ApplicationModeGame::init() {
	// Inicializar seg�n el fichero.
	char* filename = "";

	switch (mIdLevel) {
	case 0: filename = "../data/LevelEasy.json";
		break;
	case 1: filename = "../data/LevelMedium.json";
		break;
	case 2: filename = "../data/LevelHard.json";
		break;
	}

	Document document	 = loadLevel(filename);
	int numBlocks		 = document["NumBlocks"].GetInt();
	int numLives		 = document["NumLives"].GetInt();
	int numBalls		 = document["NumBalls"].GetInt();
	int numObjectLives	 = document["NumObjectLives"].GetInt();
	int numObjectBullets = document["NumObjectBullets"].GetInt();
	
	// Init game state

	// Init player
	game->InitPlayer(vmake(0, 200), vmake(12, 12), game->mTexPlayer, vmake(70, 20));
	game->mLives = numLives;

	// Init blocks
	game->InitBlocks(numBlocks);

	// Init balls
	game->InitBalls(numBalls);

	// Set Percentage NumObjectsLives-Bullets.
	game->setPercentageObjectLives(numObjectLives);
	game->setPercentageObjectBullets(numObjectBullets);

	mPauseMode->init();
}

void ApplicationModeGame::load() {
	FONT_Init();

	game->mTexbkg	    = CORE_LoadPNG("../data/background_brick.png", true);
	game->mTexsmallball = CORE_LoadPNG("../data/ballsoccer.png",	   false);
	game->mTexPlayer    = CORE_LoadPNG("../data/player.png",		   false);
	game->mTexBlock	    = CORE_LoadPNG("../data/block.png",			   false);
	game->mTexBallLive  = CORE_LoadPNG("../data/heart.png",			   false);
	game->mTexBullets   = CORE_LoadPNG("../data/bullets.png",		   false);
	game->mTexBullet    = CORE_LoadPNG("../data/bullet.png",		   false);

	mPauseMode->load();
}

void ApplicationModeGame::run() {
	if (game->GetPauseGame()) {
		mPauseMode->run();
	} else 
		game->run();
}

void ApplicationModeGame::desactivate() {
	mPauseMode->desactivate();
}

void ApplicationModeGame::unLoad() {
	CORE_UnloadPNG(game->mTexbkg);
	CORE_UnloadPNG(game->mTexPlayer);
	CORE_UnloadPNG(game->mTexsmallball);
	CORE_UnloadPNG(game->mTexBallLive);
	CORE_UnloadPNG(game->mTexBullet);
	CORE_UnloadPNG(game->mTexBullets);
	FONT_End();

	mPauseMode->unLoad();
}

void ApplicationModeGame::destroy() {
	mPauseMode->destroy();
}

void ApplicationModeGame::processInput() {
	if (game->GetPauseGame())
		mPauseMode->processInput();
	else
		inputManager->processInput();
}

void ApplicationModeGame::render() {
	// Render clear
	glClear(GL_COLOR_BUFFER_BIT);

	// Render background
	for (int i = 0; i <= SCR_WIDTH / 128; i++)
		for (int j = 0; j <= SCR_HEIGHT / 128; j++)
			CORE_RenderCenteredSprite(vmake(i * 128.f + 64.f, j * 128.f + 64.f), vmake(128.f, 128.f), game->mTexbkg);

	graphicsEngine->render();

	// Text
	std::stringstream ss;
	ss << locationManager->getText("G_Lives").c_str() << game->mLives;
	FONT_DrawString(vmake(15, SCR_HEIGHT - 30), ss.str().c_str());

	if (game->mNumBlocks == 0) {
		FONT_DrawString(vmake(SCR_WIDTH / 2 - 10, SCR_HEIGHT / 2), locationManager->getText("G_YouWin").c_str());
		SYS_Show();
		Sleep(2000);
		gestorApp->setDesiredMode(MA_MENU);
	}

	if (game->mLives == 0) {
		FONT_DrawString(vmake(SCR_WIDTH / 2 - 10, SCR_HEIGHT / 2), locationManager->getText("G_YouLose").c_str());
		SYS_Show();
		Sleep(2000);
		gestorApp->setDesiredMode(MA_MENU);
	}

	if (game->mCanShoot) {
		std::stringstream sn;
		sn << locationManager->getText("G_NumShots").c_str() << game->mNumShots;
		FONT_DrawString(vmake(15, SCR_HEIGHT - 60), sn.str().c_str());
	}

	SYS_Show();

	if (game->GetPauseGame())
		mPauseMode->render();
}

IdApplicationMode ApplicationModeGame::getIdApplicationMode() { return MA_GAME; }

Document ApplicationModeGame::loadLevel(const char* filename) {
	FILE *fp = fopen(filename, "rb");
	char readBuffer[65536];
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	Document document;
	document.ParseStream(is);

	fclose(fp);

	return document;
}