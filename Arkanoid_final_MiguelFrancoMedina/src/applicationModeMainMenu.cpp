#include "stdafx.h"
#include "applicationModeMainMenu.h"
#include "applicationGestor.h"
#include "global.h"
#include "stdafx.h"
#include "core.h"
#include "sys.h"
#include "font.h"
#include "LocationManager.h"

void ApplicationModeMainMenu::init() {
	
}

void ApplicationModeMainMenu::load() {
	FONT_Init();
}

void ApplicationModeMainMenu::run() {

}

void ApplicationModeMainMenu::processInput() {
	if (SYS_KeyPressed(SYS_KEY_1)) {
		gestorApp->setDesiredMode(MA_MENU);
		Sleep(100);
	} else if (SYS_KeyPressed(SYS_KEY_2)) {
		gestorApp->setDesiredMode(MA_OPTIONS_MENU);
		Sleep(100);
	} else if (SYS_KeyPressed(SYS_KEY_3)) {
		std::exit(0);
	}
}

void ApplicationModeMainMenu::render() {
	// Render clear
	glClear(GL_COLOR_BUFFER_BIT);

	// Render background
	/*for (int i = 0; i <= SCR_WIDTH / 128; i++)
	for (int j = 0; j <= SCR_HEIGHT / 128; j++)
	CORE_RenderCenteredSprite(vmake(i * 128.f + 64.f, j * 128.f + 64.f), vmake(128.f, 128.f), texMenu);*/

	// Text
	string title   = locationManager->getText("MM_Title");
	string newGame = locationManager->getText("MM_NewGame");
	string options = locationManager->getText("MM_Options");
	string exit	   = locationManager->getText("MM_Exit");

	FONT_DrawString(vmake(SCR_WIDTH / 2 - 150, SCR_HEIGHT / 2 + 200), title.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 100), newGame.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 30), options.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 - 40), exit.c_str());

	SYS_Show();
}

IdApplicationMode ApplicationModeMainMenu::getIdApplicationMode() { return MA_MAIN_MENU; }

void ApplicationModeMainMenu::desactivate() {
	// TODO
}

void ApplicationModeMainMenu::unLoad() {
	FONT_End();
}

void ApplicationModeMainMenu::destroy() {
	// TODO
}
