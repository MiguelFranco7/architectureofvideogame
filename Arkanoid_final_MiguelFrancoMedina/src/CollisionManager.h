#pragma once
#include <vector>
#include "core.h"
#include "ColliderUtils.h"
#include "Entity.h"

class CollisionManager {
public:

	class CollisionListener;
	class CollisionObject {
	public:
		CollisionObject(vec2 *pos, vec2 *size, float *radius, Entity::TCollision collisionType, CollisionListener *listener);
		vec2			  *mPos;
		vec2			  *mSize;
		float			  *mRadius;
		Entity::TCollision mCollisionType;
		CollisionListener *mListener;
	};
	
	class CollisionListener {
	public:
		virtual bool NotifyCollision(CollisionObject object) = 0;
	};

	virtual int Register(CollisionListener *)   = 0;
	virtual int UnRegister(CollisionListener *) = 0;

	bool CalculateCollision() { // TODO: Enviar puntero.

		for (size_t i = 0; i < listCollisionObjects.size(); i++) { // TODO: Comprobar que listener no sea null para notificar.
			for (size_t j = i + 1; j < listCollisionObjects.size(); j++) {
				if (listCollisionObjects[i]->mCollisionType == Entity::TCollision::Rect && listCollisionObjects[j]->mCollisionType == Entity::TCollision::Rect) {
					if (checkRectRect(*listCollisionObjects[i]->mPos, *listCollisionObjects[i]->mSize, *listCollisionObjects[j]->mPos, *listCollisionObjects[j]->mSize)) {
						listCollisionObjects[i]->mListener->NotifyCollision(*listCollisionObjects[j]);
						listCollisionObjects[j]->mListener->NotifyCollision(*listCollisionObjects[i]);
					}
				} else if (listCollisionObjects[i]->mCollisionType == Entity::TCollision::Circle && listCollisionObjects[j]->mCollisionType == Entity::TCollision::Circle) {
					if (checkCircleCircle(*listCollisionObjects[i]->mPos, *listCollisionObjects[i]->mRadius, *listCollisionObjects[j]->mPos, *listCollisionObjects[j]->mRadius)) {
						listCollisionObjects[i]->mListener->NotifyCollision(*listCollisionObjects[j]);
						listCollisionObjects[j]->mListener->NotifyCollision(*listCollisionObjects[i]);
					}
				} else if (listCollisionObjects[i]->mCollisionType == Entity::TCollision::Circle && listCollisionObjects[j]->mCollisionType == Entity::TCollision::Rect) {
					if (checkCircleRect(*listCollisionObjects[i]->mPos, *listCollisionObjects[i]->mRadius, *listCollisionObjects[j]->mPos, *listCollisionObjects[j]->mSize)) {
						listCollisionObjects[i]->mListener->NotifyCollision(*listCollisionObjects[j]);
						listCollisionObjects[j]->mListener->NotifyCollision(*listCollisionObjects[i]);
					}
				} else if (listCollisionObjects[i]->mCollisionType == Entity::TCollision::Rect && listCollisionObjects[j]->mCollisionType == Entity::TCollision::Circle) {
					if (checkCircleRect(*listCollisionObjects[j]->mPos, *listCollisionObjects[j]->mRadius, *listCollisionObjects[i]->mPos, *listCollisionObjects[i]->mSize)) {
						listCollisionObjects[i]->mListener->NotifyCollision(*listCollisionObjects[j]);
						listCollisionObjects[j]->mListener->NotifyCollision(*listCollisionObjects[i]);
					}
				}
			}
		}

		return true;
	}

	bool CalculateCollision(std::vector<Entity*> mListEntity) {

		for (size_t i = 0; i < mListEntity.size(); i++) { // TODO: Comprobar que listener no sea null para notificar.
			for (size_t j = i + 1; j < mListEntity.size(); j++) {
				if (mListEntity[i]->mCollisionType == Entity::TCollision::Rect && mListEntity[j]->mCollisionType == Entity::TCollision::Rect) {
					if (checkRectRect(mListEntity[i]->mPos, mListEntity[i]->mSize, mListEntity[j]->mPos, mListEntity[j]->mSize)) {
						mListEntity[i]->ReceiveMessage(mListEntity[j]->mMessage);
						mListEntity[j]->ReceiveMessage(mListEntity[i]->mMessage);
					}
				}
				else if (mListEntity[i]->mCollisionType == Entity::TCollision::Circle && mListEntity[j]->mCollisionType == Entity::TCollision::Circle) {
					if (checkCircleCircle(mListEntity[i]->mPos, mListEntity[i]->mSize.x / 2, mListEntity[j]->mPos, mListEntity[j]->mSize.x / 2)) {
						mListEntity[i]->ReceiveMessage(mListEntity[j]->mMessage);
						mListEntity[j]->ReceiveMessage(mListEntity[i]->mMessage);
					}
				}
				else if (mListEntity[i]->mCollisionType == Entity::TCollision::Circle && mListEntity[j]->mCollisionType == Entity::TCollision::Rect) {
					if (checkCircleRect(mListEntity[i]->mPos, mListEntity[i]->mSize.x/2, mListEntity[j]->mPos, mListEntity[j]->mSize)) {
						mListEntity[i]->ReceiveMessage(mListEntity[j]->mMessage);
						mListEntity[j]->ReceiveMessage(mListEntity[i]->mMessage);
					}
				}
				else if (mListEntity[i]->mCollisionType == Entity::TCollision::Rect && mListEntity[j]->mCollisionType == Entity::TCollision::Circle) {
					if (checkCircleRect(mListEntity[j]->mPos, mListEntity[j]->mSize.x / 2, mListEntity[i]->mPos, mListEntity[i]->mSize)) {
						mListEntity[i]->ReceiveMessage(mListEntity[j]->mMessage);
						mListEntity[j]->ReceiveMessage(mListEntity[i]->mMessage);
					}
				}
			}
		}

		return true;
	}

	std::vector<CollisionObject*> listCollisionObjects;
};