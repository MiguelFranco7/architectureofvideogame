#pragma once
#include "applicationModeGame.h"
#include "applicationModeMenu.h"
#include "applicationModeMainMenu.h"
#include "applicationModeOptionsMenu.h"
#include "applicationModePauseMenu.h"

class ApplicationGestor {
public:

	void init();
	void load();
	void run();
	void processInput();
	void render();

	void switchMode(IdApplicationMode id);

	void desactivate();
	void unLoad();
	void destroy();

	IApplicationMode   *getAppMode();
	IdApplicationMode   getDesiredMode();
	void			    setDesiredMode(IdApplicationMode mode);
	void			    setIdLevelSelected(int id);

private:
	IApplicationMode  *m_pModeActive;
	IdApplicationMode  m_desiredMode;
	int				   mIdLevelSelected;
};