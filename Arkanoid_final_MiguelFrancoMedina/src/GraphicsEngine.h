#pragma once
#include <vector>
#include "core.h"

class GraphicsEngine {
public:
	~GraphicsEngine();
	void render();

	class Sprite {
	public:
		Sprite(vec2 *pos, vec2 *size, GLuint *tex);

		vec2   *mPos;
		vec2   *mSize;
		GLuint *mTex;
	};

	std::vector<Sprite*> mListSprite;
};