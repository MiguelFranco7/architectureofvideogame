#include "stdafx.h"
#include "applicationModeOptionsMenu.h"
#include "applicationGestor.h"
#include "global.h"
#include "core.h"
#include "sys.h"
#include "font.h"
#include "LocationManager.h"
#include "AudioManager.h"

void ApplicationModeOptionsMenu::init() {

}

void ApplicationModeOptionsMenu::load() {
	FONT_Init();

	//texMenu = CORE_LoadPNG("../data/background_brick.png", true);
}

void ApplicationModeOptionsMenu::run() {

}

void ApplicationModeOptionsMenu::processInput() {
	if (SYS_KeyPressed(SYS_KEY_1)) {
		Sleep(100);
		// Music
		audioManager->setMusicOn();
		render();
	}

	if (SYS_KeyPressed(SYS_KEY_2)) {
		Sleep(100);
		// Efects
		audioManager->setEfectsOn();
		render();
	}

	if (SYS_KeyPressed(SYS_KEY_3)) {
		Sleep(100);
		locationManager->changeLanguaje(locationManager->getLanguajeId() + 1);
		render();
	}

	if (SYS_KeyPressed(SYS_KEY_4)) {
		Sleep(100);
		gestorApp->setDesiredMode(MA_MAIN_MENU);
	}	
}

void ApplicationModeOptionsMenu::render() {
	// Render clear
	glClear(GL_COLOR_BUFFER_BIT);

	// Text
	string title    = locationManager->getText("OM_Title");
	string music    = locationManager->getText("OM_Music") + audioManager->getStringMusicOn();
	string efects   = locationManager->getText("OM_Efects") + audioManager->getStringEfectsOn();
	string languaje = locationManager->getText("OM_Languaje") + locationManager->getLanguaje();
	string back		= locationManager->getText("OM_Back");

	FONT_DrawString(vmake(SCR_WIDTH / 2 - 150, SCR_HEIGHT / 2 + 200), title.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 100), music.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 30), efects.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 - 40), languaje.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 - 110), back.c_str());

	SYS_Show();
}

IdApplicationMode ApplicationModeOptionsMenu::getIdApplicationMode() { return MA_OPTIONS_MENU; }

void ApplicationModeOptionsMenu::desactivate() {
	// TODO
}

void ApplicationModeOptionsMenu::unLoad() {
	FONT_End();
}

void ApplicationModeOptionsMenu::destroy() {
	// TODO
}
