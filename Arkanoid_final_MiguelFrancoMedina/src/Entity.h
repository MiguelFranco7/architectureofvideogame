#pragma once
#include "core.h"
#include "GraphicsEngine.h"

class Component;

struct Message {
	virtual ~Message() {}
};

struct CollisionWithBallMessage			: public Message {};
struct CollisionWithBlockMessage		: public Message { float mPosRebound; };
struct CollisionBulletMessage			: public Message {};
struct CollisionWithBulletObjectMessage : public Message {};
struct CollisionWithBulletMessage	    : public Message {};
struct CollisionWithLiveObjectMessage	: public Message {};
struct CollisionWithPlayerMessage		: public Message { float mPos; };
struct MovePlayerMessage				: public Message { bool mRight; bool mLeft; bool mUp; bool mDown;
	MovePlayerMessage(bool right, bool left, bool up, bool down) { mRight = right; mLeft = left; mUp = up; mDown = down; }
};
struct ShootMessage						: public Message {};

// GUI Messages
struct MessagePointerMove : public Message {
	MessagePointerMove() { /*type = mtPointerMove;*/ }
	MessagePointerMove(float x, float y) {
		/*type = mtPointerMove;*/
		mX = x;
		mY = y;
	}

	float mX;
	float mY;
};
struct MessagePointerButtonDown : public Message {};
struct MessagePointerButtonUp   : public Message {};

class Entity {
public:

	enum TCollision {
		Rect,
		Circle,
		None
	};

	vec2						 mPos;
	vec2						 mSize;
	GLuint						 mGfx;
	GraphicsEngine::Sprite		*mSprite;
	TCollision					 mCollisionType;
	Message						*mMessage;

	Entity();
	~Entity();

	Entity(vec2 pos, vec2 vel, GLuint gfx, vec2 size, TCollision collisionType, Message *message) {
		mPos		   = pos;
		mGfx		   = gfx;
		mSize		   = size;
		mCollisionType = collisionType;
		mMessage	   = message;
	}

	virtual void run();
	virtual void addComponent(Component *component) { mComponents.push_back(component); }
	
	void ReceiveMessage(Message *msg);

private:
	std::vector<Component*> mComponents;
};