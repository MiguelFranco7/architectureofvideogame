#pragma once
#include "global.h"
#include "Entity.h"
#include "InputManager.h"

class CollisionManager;

class Component {
public:
	virtual void update()					  = 0;
	virtual bool ReceiveMessage(Message *msg) = 0;

	Entity *mOwner;
	int		mPriority;
};

class CBlock : public Component {
public:
	CBlock(Entity *owner) { 
		mOwner = owner; 
	}
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);
};

class CBall : public Component {
public:
	vec2 mVel;

	CBall(Entity *owner, vec2 vel) { mOwner = owner; mVel = vel; }
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);

private:
	vec2	mOldPos;
};

class CPlayer : public Component {
public:
	vec2 mVel;
	bool mRight;
	bool mLeft;
	bool mUp;
	bool mDown;

	CPlayer(Entity *owner, vec2 vel) { 
		mOwner	  = owner;
		mVel	  = vel;
		mRight	  = false;
		mLeft	  = false;
		mUp		  = false;
		mDown	  = false;
		mPriority = 2;

		inputManager->insertMovementComponent(this);
	}
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);

private:
	vec2	mOldPos;
};

class CBullet : public Component {
public:
	vec2 mVel;

	CBullet(Entity *owner, vec2 vel) { mOwner = owner; mVel = vel; }
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);

private:
	vec2	mOldPos;
};

class CBulletObject : public Component {
public:
	vec2 mVel;

	CBulletObject(Entity *owner, vec2 vel) { mOwner = owner; mVel = vel; }
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);

private:
	vec2	mOldPos;
};

class CLiveObject : public Component {
public:
	vec2 mVel;

	CLiveObject(Entity *owner, vec2 vel) { mOwner = owner; mVel = vel; }
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);

private:
	vec2	mOldPos;
};

class CGunObject : public Component {
public:
	CGunObject(Entity *owner) { mOwner = owner; }
	virtual void update();
	virtual bool ReceiveMessage(Message *msg);
};