#include "stdafx.h"
#include "Game.h"
#include "EntityLiveObject.h"
#include "EntityBulletObject.h"
#include "EntityBlock.h"
#include "EntityBall.h"
#include "EntityBullet.h"
#include "global.h"
#include "GraphicsEngine.h"
#include "Component.h"

void Game::run() {
	for (auto it = mListEntity.begin(); it != mListEntity.end(); it++)
		(*it)->run();

	CalculateCollision(mListEntity);

	for (auto it = mListToDelete.begin(); it != mListToDelete.end(); it++) {
		for (auto it1 = mListEntity.begin(); it1 != mListEntity.end(); it1++) {
			if (*it == *it1) {
				it1 = mListEntity.erase(it1);
				break;
			}
		}

		for (auto it2 = listCollisionObjects.begin(); it2 != listCollisionObjects.end(); it2++) {
			if (*it == dynamic_cast<Entity*>((*it2)->mListener)) {
				it2 = listCollisionObjects.erase(it2);
				break;
			}
		}

		for (auto it3 = graphicsEngine->mListSprite.begin(); it3 != graphicsEngine->mListSprite.end(); it3++) {
			if ((*it)->mSprite == *it3) {
				it3 = graphicsEngine->mListSprite.erase(it3);
				break;
			}
		}
	}

	mListToDelete.clear();

	generateLivesObject();
	generateBulletsObject();
}

void Game::setInput(bool r, bool l, bool u, bool d, bool s) {
	mPlayer->ReceiveMessage(new MovePlayerMessage(r, l, u, d));
	mSpace = s;
}

void Game::InitPlayer(vec2 pos, vec2 speed, GLuint gfx, vec2 size) {
	//mPlayer = new EntityPlayer(pos, speed, gfx, size);
	//mListEntity.push_back(mPlayer);
	////listCollisionObjects.push_back(CollisionObject(&mPlayer->mPos, &mPlayer->mSize, nullptr, Rect, mPlayer->mType, mPlayer));
	//GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&mPlayer->mPos, &mPlayer->mSize, &mPlayer->mGfx);
	//graphicsEngine->mListSprite.push_back(sprite);
	//mListEntity[mListEntity.size() - 1]->mSprite = sprite;

	mPlayer = new Entity(pos, speed, gfx, size, Entity::TCollision::Rect, new CollisionWithPlayerMessage());
	CPlayer *cPlayer = new CPlayer(mPlayer, speed);
	mPlayer->addComponent(cPlayer);
	inputManager->insertMovementComponent(cPlayer);
	mListEntity.push_back(mPlayer);

	GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&mPlayer->mPos, &mPlayer->mSize, &mPlayer->mGfx);
	graphicsEngine->mListSprite.push_back(sprite);
	mListEntity[mListEntity.size() - 1]->mSprite = sprite;
}

void Game::InitBlocks(int numBlocks) {
	mNumBlocks = numBlocks;

	/*for (int i = 0; i < numBlocks; i++) {
		EntityBlock *block = new EntityBlock(vmake(CORE_FRand(0, SCR_WIDTH + 20), CORE_FRand(200, SCR_HEIGHT - 20)), vmake(-10, -10), mTexBlock, vmake(30, 30));
		mListEntity.push_back(block);
		listCollisionObjects.push_back(CollisionObject(&block->mPos, &block->mSize, nullptr, Rect, block->mType, block));
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&block->mPos, &block->mSize, &block->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
	}*/

	int x = 0;
	int y = SCR_HEIGHT - 50;
	for (int i = 0; i < numBlocks; i++) {
		// Posicionamiento de los bloques
		x += 50;
		if (x > SCR_WIDTH) {
			x = 50;
			y -= 50;
		}

		//Entity *block = new Entity(vmake(CORE_FRand(0, SCR_WIDTH + 20), CORE_FRand(200, SCR_HEIGHT - 20)), vmake(-10, -10), mTexBlock, vmake(30, 30), Entity::TCollision::Rect, new CollisionWithBlockMessage());
		Entity *block = new Entity(vmake(x, y), vmake(-10, -10), mTexBlock, vmake(30, 30), Entity::TCollision::Rect, new CollisionWithBlockMessage());
		block->addComponent(new CBlock(block));
		mListEntity.push_back(block);
		
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&block->mPos, &block->mSize, &block->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
	}
}

void Game::InitBalls(int numBalls) {
	/*for (int i = 0; i < numBalls; i++) {
		EntityBall *ball = new EntityBall(vmake(CORE_FRand(0, SCR_WIDTH + 20), CORE_FRand(400, SCR_HEIGHT - 20)), vmake(-10, -10), mTexsmallball, 16.f);
		mListEntity.push_back(ball);
		listCollisionObjects.push_back(CollisionObject(&ball->mPos, nullptr, &ball->mRadius, Circle, ball->mType, ball));
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&ball->mPos, &ball->mSize, &ball->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
	}*/

	for (int i = 0; i < numBalls; i++) {
		Entity *ball = new Entity(vmake(CORE_FRand(0, SCR_WIDTH + 20), CORE_FRand(400, SCR_HEIGHT - 20)), vmake(-10, -10), mTexsmallball, vmake(16.f, 16.f), Entity::TCollision::Circle, new CollisionWithBallMessage());
		ball->addComponent(new CBall(ball, vmake(-10, -10)));
		mListEntity.push_back(ball);
		
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&ball->mPos, &ball->mSize, &ball->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
	}
}

void Game::SetpauseGame(bool pause) {
	mPauseGame = pause;
}

bool Game::GetPauseGame() {
	return mPauseGame;
}

void Game::generateLivesObject() {
	int ran = CORE_FRand(0.f, mPercentageObjectLives);
	if (ran == 1) {
		//EntityLiveObject *live = new EntityLiveObject(vmake(CORE_FRand(30, SCR_WIDTH - 30), SCR_HEIGHT), vmake(-7, -7), mTexBallLive, 16.f);
		//mListEntity.push_back(live);
		////listCollisionObjects.push_back(CollisionObject(&live->mPos, nullptr, &live->mRadius, Circle, live->mType, live));
		//GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&live->mPos, &live->mSize, &live->mGfx);
		//graphicsEngine->mListSprite.push_back(sprite);
		//mListEntity[mListEntity.size() - 1]->mSprite = sprite;

		Entity *live = new Entity(vmake(CORE_FRand(30, SCR_WIDTH - 30), SCR_HEIGHT), vmake(-7, -7), mTexBallLive, vmake(16.f, 16.f), Entity::TCollision::Circle, new CollisionWithLiveObjectMessage());
		live->addComponent(new CLiveObject(live, vmake(-10, -10)));
		mListEntity.push_back(live);
		
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&live->mPos, &live->mSize, &live->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
	}
}

void Game::generateBulletsObject() {
	int ran = CORE_FRand(0.f, mPercentageObjectBullets);
	if (ran == 1) {
		//EntityBulletObject *bullets = new EntityBulletObject(vmake(CORE_FRand(30, SCR_WIDTH - 30), SCR_HEIGHT), vmake(-7, -7), mTexBullets, vmake(25.f, 25.f));
		//mListEntity.push_back(bullets);
		////listCollisionObjects.push_back(CollisionObject(&bullets->mPos, &bullets->mSize, nullptr, Rect, bullets->mType, bullets));
		//GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&bullets->mPos, &bullets->mSize, &bullets->mGfx);
		//graphicsEngine->mListSprite.push_back(sprite);
		//mListEntity[mListEntity.size() - 1]->mSprite = sprite;

		Entity *bullets = new Entity(vmake(CORE_FRand(30, SCR_WIDTH - 30), SCR_HEIGHT), vmake(-7, -7), mTexBullets, vmake(25.f, 25.f), Entity::TCollision::Rect, new CollisionWithBulletObjectMessage());
		bullets->addComponent(new CBulletObject(bullets, vmake(-10, -10)));
		mListEntity.push_back(bullets);
		
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&bullets->mPos, &bullets->mSize, &bullets->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
	}
}

void Game::shoot() {
	if (mCanShoot && mNumShots > 0) {
		//EntityBullet *bullet = new EntityBullet(mPlayer->mPos, vmake(20, 20), mTexBullet, vmake(20.f, 20.f));
		//mListEntity.push_back(bullet);
		////listCollisionObjects.push_back(CollisionObject(&bullet->mPos, &bullet->mSize, nullptr, Rect, bullet->mType, bullet));
		//GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&bullet->mPos, &bullet->mSize, &bullet->mGfx);
		//graphicsEngine->mListSprite.push_back(sprite);
		//mListEntity[mListEntity.size() - 1]->mSprite = sprite;
		//mNumShots--;

		Entity *bullet = new Entity(mPlayer->mPos, vmake(20, 20), mTexBullet, vmake(20.f, 20.f), Entity::TCollision::Rect, new CollisionWithBulletMessage());
		bullet->addComponent(new CBullet(bullet, vmake(-20, -20)));
		mListEntity.push_back(bullet);
		
		GraphicsEngine::Sprite *sprite = new GraphicsEngine::Sprite(&bullet->mPos, &bullet->mSize, &bullet->mGfx);
		graphicsEngine->mListSprite.push_back(sprite);
		mListEntity[mListEntity.size() - 1]->mSprite = sprite;
		mNumShots--;
	}
}
