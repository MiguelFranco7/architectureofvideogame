#include "stdafx.h"
#include "Entity.h"
#include "Component.h"

Entity::Entity() {}

Entity::~Entity() {
	mComponents.~vector();
}

void Entity::run() {
	for (auto it = mComponents.begin(); it != mComponents.end(); it++)
		(*it)->update();
}

void Entity::ReceiveMessage(Message * msg) {
	for (auto it = mComponents.begin(); it != mComponents.end(); it++) {
		(*it)->ReceiveMessage(msg);
	}
}
