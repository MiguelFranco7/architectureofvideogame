#include "stdafx.h"
#include "ColliderUtils.h"
#include <minmax.h>

template<class T>
const T clamp(const T& x, const T& upper, const T& lower);

bool checkCircleCircle(const vec2& pos1, float radius1,
	const vec2& pos2, float radius2) {
	int dx = pos2.x - pos1.x;
	int dy = pos2.y - pos1.y;
	int rad = radius1 + radius2;

	if ((dx * dx) + (dy * dy) < rad * rad)
		return true;
	else
		return false;
}

bool checkCircleRect(const vec2& circlePos, float circleRadius,
	const vec2& rectPos, const vec2& rectSize) {
	float closestX = clamp(circlePos.x, rectPos.x, rectPos.x + rectSize.x);
	float closestY = clamp(circlePos.y, rectPos.y, rectPos.y + rectSize.y);	float distanceX = circlePos.x - closestX;	float distanceY = circlePos.y - closestY;
	if ((distanceX * distanceX) + (distanceY * distanceY) < circleRadius * circleRadius)
		return true;
	else
		return false;
}

bool checkRectRect(const vec2& rectPos1, const vec2& rectSize1,
	const vec2& rectPos2, const vec2& rectSize2) {

	if ((max(rectPos1.x, rectPos2.x) < min(rectPos1.x + rectSize1.x, rectPos2.x + rectSize2.x)) &&
		(max(rectPos1.y, rectPos2.y) < min(rectPos1.y + rectSize1.y, rectPos2.y + rectSize2.y))) {
		return true;
	} else return false;
}

//bool checkCirclePixels(const vec2& circlePos, float circleRadius,
//	const vec2& pixelsPos, const vec2& pixelsSize, const uint8_t* pixels) {
//	
//	bool colision = false;
//	int x0 = max(pixelsPos.x, circlePos.x - circleRadius);
//	int x1 = min(pixelsPos.x + pixelsSize.x, circlePos.x + circleRadius);
//	int y0 = max(pixelsPos.y, circlePos.y - circleRadius);
//	int y1 = min(pixelsPos.y + pixelsSize.y, circlePos.y + circleRadius);
//
//	if (x0 < x1 && y0 < y1) { // CheckRectRect
//		for (int row = y0; row < y1 && !colision; ++row) {
//			for (int col = x0; col < x1 && !colision; ++col) {
//
//				vec2 posReal = vmake(col, row);
//				vec2 pos = posReal - pixelsPos;
//				int position = (pos.y * static_cast<int>(pixelsSize.x) + pos.x) * 4 + 3;
//				int alpha = pixels[position];
//
//				if (posReal.distance(circlePos) < circleRadius && alpha >= 128) {
//					colision = true;
//				}
//			}
//		}
//		return colision;
//	} else {
//		return colision;
//	}
//}
//
//bool checkPixelsPixels(
//	const vec2& pixelsPos1, const vec2& pixelsSize1, const uint8_t* pixels1,
//	const vec2& pixelsPos2, const vec2& pixelsSize2, const uint8_t* pixels2) {
//
//	bool colision = false;
//	int x0 = max(pixelsPos1.x, pixelsPos2.x);
//	int x1 = min(pixelsPos1.x + pixelsSize1.x, pixelsPos2.x + pixelsSize2.x);
//	int y0 = max(pixelsPos1.y, pixelsPos2.y);
//	int y1 = min(pixelsPos1.y + pixelsSize1.y, pixelsPos2.y + pixelsSize2.y);
//
//	if (x0 < x1 && y0 < y1) { // CheckRectRect
//		for (int row = y0; row < y1 && !colision; ++row) {
//			for (int col = x0; col < x1 && !colision; ++col) {
//
//				vec2 pos1 = vmake(col - pixelsPos1.x, row - pixelsPos1.y);
//				vec2 pos2 = vmake(col - pixelsPos2.x, row - pixelsPos2.y);
//
//				int position1 = (pos1.y * static_cast<int>(pixelsSize1.x) + pos1.x) * 4 + 3;
//				int position2 = (pos2.y * static_cast<int>(pixelsSize2.x) + pos2.x) * 4 + 3;
//
//				int al1 = pixels1[position1];
//				int al2 = pixels2[position2];
//
//				if (al1 >= 128 && al2 >= 128) {
//					colision = true;
//				}
//			}
//		}
//		return colision;
//	} else {
//		return colision;
//	}
//}
//
//bool checkPixelsRect(
//	const vec2& pixelsPos, const vec2& pixelsSize, const uint8_t* pixels,
//	const vec2& rectPos, const vec2& rectSize) {
//	
//	bool colision = false;
//	int x0 = max(pixelsPos.x, rectPos.x);
//	int x1 = min(pixelsPos.x + pixelsSize.x, rectPos.x + rectSize.x);
//	int y0 = max(pixelsPos.y, rectPos.y);
//	int y1 = min(pixelsPos.y + pixelsSize.y, rectPos.y + rectSize.y);
//
//	if (x0 < x1 && y0 < y1) { // CheckRectRect
//		for (int row = y0; row < y1 && !colision; ++row) {
//			for (int col = x0; col < x1 && !colision; ++col) {
//
//				vec2 pos = vmake(col - pixelsPos.x, row - pixelsPos.y);
//				int position = (pos.y * static_cast<int>(pixelsSize.x) + pos.x) * 4 + 3;
//				int alpha = pixels[position];
//
//				if (alpha >= 128) {
//					colision = true;
//				}
//			}
//		}
//		return colision;
//	} else {
//		return colision;
//	}
//}

template<class T>
const T clamp(const T& val, const T& min, const T& max) {
	return max(min, min(max, val));
}