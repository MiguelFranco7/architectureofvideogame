#pragma once
#include <iostream>
#include "core.h"

bool checkCircleCircle(const vec2& pos1, float radius1,
	const vec2& pos2, float radius2);

bool checkCircleRect(const vec2& circlePos, float circleRadius,
	const vec2& rectPos, const vec2& rectSize);

bool checkRectRect(const vec2& rectPos1, const vec2& rectSize1,
	const vec2& rectPos2, const vec2& rectSize2);

//bool checkCirclePixels(const vec2& circlePos, float circleRadius,
//	const vec2& pixelsPos, const vec2& pixelsSize, const uint8_t* pixels);

//bool checkPixelsPixels(
//	const vec2& pixelsPos1, const vec2& pixelsSize1, const uint8_t* pixels1,
//	const vec2& pixelsPos2, const vec2& pixelsSize2, const uint8_t* pixels2);

//bool checkPixelsRect(
//	const vec2& pixelsPos, const vec2& pixelsSize, const uint8_t* pixels,
//	const vec2& rectPos, const vec2& rectSize);