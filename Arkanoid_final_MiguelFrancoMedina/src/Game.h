#pragma once
#include "core.h"
#include "sys.h"
#include "ColliderUtils.h"
#include "EntityPlayer.h"

class Game : public CollisionManager {
public:
	Game() {}

	void run();
	void setInput	 (bool right, bool left, bool up, bool down, bool space);
	void InitPlayer  (vec2 pos, vec2 speed, GLuint gfx, vec2 size);
	void InitBlocks  (int numBlocks);
	void InitBalls	 (int numBalls);
	void SetpauseGame(bool pause);
	bool GetPauseGame();

	void generateLivesObject();
	void generateBulletsObject();

	void shoot();

	std::vector<Entity*>			mListEntity;
	std::vector<CollisionListener*> mListCollisionListener;
	std::vector<Entity*>			mListToDelete;

	virtual int UnRegister(CollisionListener *) { return 3; }

	void setPercentageObjectLives(int lives) { mPercentageObjectLives = lives; }
	void setPercentageObjectBullets(int bullets) { mPercentageObjectBullets = bullets; }

	virtual int Register(CollisionListener * listener) {
		mListCollisionListener.push_back(listener);
		return 3;
	}

	int  mLives;
	bool mCanShoot;
	int  mNumShots;
	int  mNumBlocks;

	// Textures
	GLuint mTexbkg;
	GLuint mTexsmallball;
	GLuint mTexPlayer;
	GLuint mTexBlock;
	GLuint mTexBallLive;
	GLuint mTexBullets;
	GLuint mTexBullet;

	Entity *mPlayer;
	bool	mSpace;

private:
	int  mPercentageObjectLives;
	int  mPercentageObjectBullets;
	bool mPauseGame;
};