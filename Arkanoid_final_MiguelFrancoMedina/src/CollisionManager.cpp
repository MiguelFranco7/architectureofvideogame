#include "stdafx.h"
#include "CollisionManager.h"

CollisionManager::CollisionObject::CollisionObject(vec2 * pos, vec2 * size, float * radius, Entity::TCollision collisionType, CollisionListener * listener) {
	mPos		   = pos;
	mSize		   = size;
	mRadius		   = radius;
	mCollisionType = collisionType;
	mListener	   = listener;
}
