#pragma once
#include "global.h"
#include "Game.h"

class Component;

using namespace std;

class InputManager {
public:
	void processInput();

	void insertMovementComponent(Component *component);
	void deleteMovementComponent(Component *component);
	void insertShootComponent   (Component *component);
	void deleteShootComponent	(Component *component);
	void insertOption1			(Component *component);
	void deleteOption1			(Component *component);
	void insertOption2			(Component *component);
	void deleteOption2			(Component *component);
	void insertOption3			(Component *component);
	void deleteOption3			(Component *component);
	void insertOption4			(Component *component);
	void deleteOption4			(Component *component);

private:
	void notifyMovementComponents(bool r, bool l, bool u, bool d);
	void notifyShootComponents  ();
	void notifyOption1Components();
	void notifyOption2Components();
	void notifyOption3Components();
	void notifyOption4Components();

	vector<Component*> listMovement;
	vector<Component*> listShoot;
	vector<Component*> listOption1;
	vector<Component*> listOption2;
	vector<Component*> listOption3;
	vector<Component*> listOption4;
};
