#include "stdafx.h"
#include "applicationModePauseMenu.h"
#include "applicationGestor.h"
#include "global.h"
#include "core.h"
#include "sys.h"
#include "font.h"
#include "LocationManager.h"

void ApplicationModePauseMenu::init() {

}

void ApplicationModePauseMenu::load() {
	FONT_Init();

	//texMenu = CORE_LoadPNG("../data/background_brick.png", true);
}

void ApplicationModePauseMenu::run() {

}

void ApplicationModePauseMenu::processInput() {
	if (SYS_KeyPressed(SYS_KEY_1))
		game->SetpauseGame(false);

	if (SYS_KeyPressed(SYS_KEY_2)) {
		gestorApp->setDesiredMode(MA_MAIN_MENU);
		Sleep(100);
	}

}

void ApplicationModePauseMenu::render() {
	// Render clear
	//glClear(GL_COLOR_BUFFER_BIT);

	// Text
	string title  = locationManager->getText("PM_Title");
	string resume = locationManager->getText("PM_Resume");
	string exit   = locationManager->getText("PM_Exit");

	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200 + 20, SCR_HEIGHT / 2 + 50), title.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200 + 20, SCR_HEIGHT / 2), resume.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200 + 20, SCR_HEIGHT / 2 - 30), exit.c_str());

	SYS_Show();
}

IdApplicationMode ApplicationModePauseMenu::getIdApplicationMode() { return MA_PAUSE_MENU; }

void ApplicationModePauseMenu::desactivate() {
	// TODO
}

void ApplicationModePauseMenu::unLoad() {
	FONT_End();
}

void ApplicationModePauseMenu::destroy() {
	// TODO
}
