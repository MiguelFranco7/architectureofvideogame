#include "stdafx.h"
#include "applicationGestor.h"

void ApplicationGestor::init()		   { m_pModeActive->init(); }
void ApplicationGestor::load()		   { m_pModeActive->load(); }
void ApplicationGestor::run()		   { m_pModeActive->run(); }
void ApplicationGestor::processInput() { m_pModeActive->processInput(); }
void ApplicationGestor::render()	   { m_pModeActive->render(); }

void ApplicationGestor::switchMode(IdApplicationMode id) {
	switch (id) {
	case MA_GAME:
		m_pModeActive = new ApplicationModeGame(mIdLevelSelected);
		m_desiredMode = MA_GAME;
		break;
	case MA_MENU:
		m_pModeActive = new ApplicationModeMenu();
		m_desiredMode = MA_MENU;
		break;
	case MA_MAIN_MENU:
		m_pModeActive = new ApplicationModeMainMenu();
		m_desiredMode = MA_MAIN_MENU;
		break;
	case MA_OPTIONS_MENU:
		m_pModeActive = new ApplicationModeOptionsMenu();
		m_desiredMode = MA_OPTIONS_MENU;
		break;
	case MA_PAUSE_MENU:
		m_pModeActive = new ApplicationModePauseMenu();
		m_desiredMode = MA_PAUSE_MENU;
		break;
	}
}

void ApplicationGestor::desactivate() { m_pModeActive->desactivate(); }
void ApplicationGestor::unLoad()	  { m_pModeActive->unLoad(); }
void ApplicationGestor::destroy()	  { m_pModeActive->destroy(); }

IApplicationMode   *ApplicationGestor::getAppMode()							  { return m_pModeActive; }
IdApplicationMode   ApplicationGestor::getDesiredMode()						  { return m_desiredMode; }
void			    ApplicationGestor::setDesiredMode(IdApplicationMode mode) { m_desiredMode = mode; }
void			    ApplicationGestor::setIdLevelSelected(int id)			  { mIdLevelSelected = id; }
