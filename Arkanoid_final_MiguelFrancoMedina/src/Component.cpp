#include "stdafx.h"
#include "Component.h"
#include "global.h"
#include "Game.h"

void CBlock::update() {}

bool CBlock::ReceiveMessage(Message *msg) {
	CollisionWithBallMessage *ballMsg = dynamic_cast<CollisionWithBallMessage*>(msg);
	if (ballMsg) {
		game->mListToDelete.push_back(mOwner);
		game->mNumBlocks--;
	} else {
		CollisionWithBulletMessage *bulletMsg = dynamic_cast<CollisionWithBulletMessage*>(msg);
		if (bulletMsg) {
			game->mListToDelete.push_back(mOwner);
			game->mNumBlocks--;
		}
	}

	return false;
}

void CBall::update() {
	mOldPos = mOwner->mPos;
	vec2 newpos = vadd(mOldPos, mVel);

	bool collision = false;

	mOwner->mPos = newpos;
	// Rebound on margins
	if (mVel.x > 0.0) {
		if (mOwner->mPos.x > SCR_WIDTH)
			mVel.x *= -1.0;
	} else {
		if (mOwner->mPos.x < 0)
			mVel.x *= -1.0;
	}
	if (mVel.y > 0.0) {
		if (mOwner->mPos.y > SCR_HEIGHT)
			mVel.y *= -1.0;
	} else {
		if (mOwner->mPos.y < 0) {
			mVel.y *= -1.0;
			game->mLives--;
		}
	}
}

bool CBall::ReceiveMessage(Message * msg) {
	CollisionWithBlockMessage *blockMsg = dynamic_cast<CollisionWithBlockMessage*>(msg);
	if (blockMsg) {
		/*if (blockMsg->mPosRebound <= 25)
			mVel.x = -5.f;
		else if (blockMsg->mPosRebound > 25 && blockMsg->mPosRebound <= 75)
			mVel.x = 0.f;
		else if (blockMsg->mPosRebound >= 75)
			mVel.x = 5.f;*/

		mVel.x = rand() % 20 - 10;	

		mVel.y *= -1.f;
		mOwner->mPos = vadd(mOldPos, mVel);
	} else {
		CollisionWithPlayerMessage *playerMsg = dynamic_cast<CollisionWithPlayerMessage*>(msg);
		if (playerMsg) {
			/*if (playerMsg->mPos <= 25)
				mVel.x = -5.f;
			else if (playerMsg->mPos > 25 && playerMsg->mPos <= 75)
				mVel.x = 0.f;
			else if (playerMsg->mPos <= 25)
				mVel.x = 5.f;*/

			mVel.x = rand() % 20 - 10;

			mVel.y *= -1.f;
			mOwner->mPos = vadd(mOldPos, mVel);
		}
	}

	return false;
}

void CPlayer::update() {
	if (mUp)
		mOwner->mPos = vmake(mOwner->mPos.x, mOwner->mPos.y + 50);

	if (mDown)
		mOwner->mPos = vmake(mOwner->mPos.x, mOwner->mPos.y - 50);

	if (mLeft)
		mOwner->mPos = vmake(mOwner->mPos.x - 50, mOwner->mPos.y);

	if (mRight)
		mOwner->mPos = vmake(mOwner->mPos.x + 50, mOwner->mPos.y);
}

bool CPlayer::ReceiveMessage(Message * msg) {
	bool consumido = false;

	MovePlayerMessage *playerMsg = dynamic_cast<MovePlayerMessage*>(msg);
	if (playerMsg) {
		consumido = true;

		if (playerMsg->mUp)
			mOwner->mPos = vmake(mOwner->mPos.x, mOwner->mPos.y + mVel.y);

		if (playerMsg->mDown)
			mOwner->mPos = vmake(mOwner->mPos.x, mOwner->mPos.y - mVel.y);

		if (playerMsg->mLeft)
			mOwner->mPos = vmake(mOwner->mPos.x - mVel.x, mOwner->mPos.y);

		if (playerMsg->mRight)
			mOwner->mPos = vmake(mOwner->mPos.x + mVel.x, mOwner->mPos.y);
	}

	return consumido;
}

void CBullet::update() {
	mOwner->mPos.y += 10;

	if (mOwner->mPos.y > SCR_HEIGHT)
		game->mListToDelete.push_back(mOwner);
}

bool CBullet::ReceiveMessage(Message * msg) {
	CollisionWithBlockMessage *blockMsg = dynamic_cast<CollisionWithBlockMessage*>(msg);
	if (blockMsg)
		game->mListToDelete.push_back(mOwner);

	return false;
}

void CBulletObject::update() {
	mOwner->mPos.y--;

	if (mOwner->mPos.y < 0)
		game->mListToDelete.push_back(mOwner);
}

bool CBulletObject::ReceiveMessage(Message * msg) {
	CollisionWithPlayerMessage *playerMsg = dynamic_cast<CollisionWithPlayerMessage*>(msg);
	if (playerMsg) {
		game->mListToDelete.push_back(mOwner);
		game->mCanShoot = true;
		game->mNumShots = 1;

		CGunObject *gunObject = new CGunObject(game->mPlayer);
		inputManager->insertShootComponent(gunObject);
		game->mPlayer->addComponent(gunObject);
	}

	return false;
}

void CLiveObject::update() {
	mOwner->mPos.y--;

	if (mOwner->mPos.y < 0)
		game->mListToDelete.push_back(mOwner);
}

bool CLiveObject::ReceiveMessage(Message * msg) {
	CollisionWithPlayerMessage *playerMsg = dynamic_cast<CollisionWithPlayerMessage*>(msg);
	if (playerMsg) {
		game->mListToDelete.push_back(mOwner);
		game->mLives++;
	}

	return false;
}

void CGunObject::update() {

}

bool CGunObject::ReceiveMessage(Message * msg) {
	ShootMessage *shootMsg = dynamic_cast<ShootMessage*>(msg);
	if (shootMsg)
		game->shoot();

	return false;
}
