#include "stdafx.h"
#include "applicationGestor.h"
#include "LocationManager.h"
#include "AudioManager.h"
#include "global.h"

ApplicationGestor *gestorApp	   = new ApplicationGestor();
Game			  *game			   = nullptr;
InputManager	  *inputManager	   = nullptr;
GraphicsEngine    *graphicsEngine  = nullptr;
LocationManager   *locationManager = new LocationManager(0);
AudioManager	  *audioManager	   = new AudioManager();

void setUpRendering();

int Main(void) {
	
	// Set up rendering
	setUpRendering();

	// Load lamguaje
	locationManager->changeLanguaje(0);

	gestorApp->switchMode(MA_MAIN_MENU);
	gestorApp->load();
	gestorApp->init();

	while (/*!SYS_GottaQuit()*/ !SYS_KeyPressed(SYS_KEY_ESC)) {
		if (gestorApp->getDesiredMode() != gestorApp->getAppMode()->getIdApplicationMode()) {
			gestorApp->desactivate();
			gestorApp->unLoad();
			gestorApp->destroy();

			gestorApp->switchMode(gestorApp->getDesiredMode());
			gestorApp->load();
			gestorApp->init();
		}
		
		gestorApp->processInput();
		gestorApp->run();
		gestorApp->render();
	
		// Keep system running
		SYS_Pump();
		SYS_Sleep(17);
	}

	return 0;
}

void setUpRendering() {
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT); // Sets up clipping
	glClearColor(0.0f, 0.1f, 0.3f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, SCR_WIDTH, 0.0, SCR_HEIGHT, 0.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}