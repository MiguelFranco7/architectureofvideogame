#include "stdafx.h"
#include "LocationManager.h"
#pragma warning(disable:4996) // Using open/close/read... for file access

using namespace rapidjson;

LocationManager::LocationManager(int languajeId) {
	mLanguajeId = languajeId;
}

void LocationManager::changeLanguaje(int languajeId) {
	char* filename = "";
	mLanguajeId	   = languajeId;

	switch (languajeId) {
	case 0: filename = "../data/lang_english.json";
		break;
	case 1: filename = "../data/lang_spanish.json";
		break;
	default: filename = "../data/lang_english.json";
		mLanguajeId = 0;
		break;
	}

	Document document = loadLanguajeJson(filename);

	mMapLanguaje.clear();

	mMapLanguaje.insert({ "MM_Title",    document["MM_Title"].GetString() });
	mMapLanguaje.insert({ "MM_NewGame",  document["MM_NewGame"].GetString() });
	mMapLanguaje.insert({ "MM_Options",  document["MM_Options"].GetString() });
	mMapLanguaje.insert({ "MM_Exit",     document["MM_Exit"].GetString() });
	mMapLanguaje.insert({ "OM_Title",    document["OM_Title"].GetString() });
	mMapLanguaje.insert({ "OM_Music",    document["OM_Music"].GetString() });
	mMapLanguaje.insert({ "OM_Efects",   document["OM_Efects"].GetString() });
	mMapLanguaje.insert({ "OM_Languaje", document["OM_Languaje"].GetString() });
	mMapLanguaje.insert({ "OM_Back",     document["OM_Back"].GetString() });
	mMapLanguaje.insert({ "SG_Title",    document["SG_Title"].GetString() });
	mMapLanguaje.insert({ "SG_Easy",     document["SG_Easy"].GetString() });
	mMapLanguaje.insert({ "SG_Medium",   document["SG_Medium"].GetString() });
	mMapLanguaje.insert({ "SG_Hard",     document["SG_Hard"].GetString() });
	mMapLanguaje.insert({ "SG_Back",     document["SG_Back"].GetString() });
	mMapLanguaje.insert({ "PM_Title",    document["PM_Title"].GetString() });
	mMapLanguaje.insert({ "PM_Resume",   document["PM_Resume"].GetString() });
	mMapLanguaje.insert({ "PM_Exit",     document["PM_Exit"].GetString() });
	mMapLanguaje.insert({ "G_Lives",     document["G_Lives"].GetString() });
	mMapLanguaje.insert({ "G_NumShots",  document["G_NumShots"].GetString() });
	mMapLanguaje.insert({ "G_YouWin",    document["G_YouWin"].GetString() });
	mMapLanguaje.insert({ "G_YouLose",   document["G_YouLose"].GetString() });
}

Document LocationManager::loadLanguajeJson(const char* filename) {
	FILE *fp = fopen(filename, "rb");
	char readBuffer[65536];
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	Document document;
	document.ParseStream(is);

	fclose(fp);

	return document;
}

string LocationManager::getText(string key) {
	return mMapLanguaje.find(key)->second;
}

string LocationManager::getLanguaje() {
	string lang = "ENGLISH";

	switch (mLanguajeId) {
	case 0: lang = "ENGLISH";
		break;
	case 1: lang = "SPANISH";
		break;
	}

	return lang;
}

int LocationManager::getLanguajeId() {
	return mLanguajeId;
}
