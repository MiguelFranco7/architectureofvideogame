#pragma once
#include <string>
#include "../Entity.h"
#include "../IEventListener.h"

class Control {
public:
	// Input del usuario que vendr� de nuestro InputManager
	void InjectInput(const Message& message);

	// Cada tipo de control tendr� su m�todo de actualizaci�n
	virtual void update() = 0;

	// Cada tipo de control implementar� su render
	virtual void render() = 0;

	// Se implementar� como se responde a los eventos de entrada para cada tipo de control
	virtual void onInputEvent(const Message& message) = 0;

	// Establecemos a donde notifican los controles sus eventos
	void setEventListener(IEventListener* eventListener);

	//string mName;
	vec2 mPosition;
	vec2 mSize;
	std::vector<IEventListener*> mEventListeners;
	bool mVisible;
};