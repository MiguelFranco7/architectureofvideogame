#pragma once
#include <string>
#include <vector>
#include "Control.h"

class Button : public Control {
public:
	Button();

	//bool init(const string name, const vec2& position, const string& normalImage, const string& pushImage);

	void		 update();
	void		 render();
	virtual void onInputEvent(const Message& message);

protected:
	//Image* mNormalImage;
	//Image* mPushImage;
	bool mPushed;
};