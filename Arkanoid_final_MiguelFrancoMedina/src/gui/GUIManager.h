#pragma once

class Control;
class Message;

class GUIManager {
	// Establece el control padre o "desktop"
	void setRootControl(Control* control);

	// Llama a los updates de todos los controles
	void update();

	// Llama a los render de todos los controles
	void render();

	// Punto de entrada del input; desde aqu� se enruta hacia el control adecuado
	void injectInput(const Message& message);
};
