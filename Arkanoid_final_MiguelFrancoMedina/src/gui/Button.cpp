#include "../stdafx.h"
#include "Button.h"

Button::Button() {
	mPushed = false;
}

/* Button::init(const string name, const vec2 & position, const string & normalImage, const string & pushImage) {
	mName = name;
	mPosition = position;
	mNormalImage = new Image(normalImage.c_str());
	mPushImage = new Image(pushImage.c_str());
	mSize = vec2((float)mNormalImage->getWith(), (float)mNormalImage->getHeight());
	
	return true;
}*/

void Button::update() {

}

void Button::render() {
	if (mVisible) {
		
	}
}

void Button::onInputEvent(const Message& message) {
	// TODO
	const MessagePointerButtonDown *msgDown = dynamic_cast<const MessagePointerButtonDown*>(&message);
	if (msgDown) {
		mPushed = true;
	} else {
		const MessagePointerButtonUp *msgUp = dynamic_cast<const MessagePointerButtonUp*>(&message);
		if (msgUp) {
			std::vector<IEventListener*>::const_iterator it = mEventListeners.begin();
			while (it != mEventListeners.end()) {
				IEventListener *listener = *it;
				listener->onClick(this);
				it++;
			}

			mPushed = false;
		}
	}
}
