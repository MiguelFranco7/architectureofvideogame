#include "stdafx.h"
#include "EntityBullet.h"
#include "global.h"
#include "Game.h"

void EntityBullet::run() {
	mPos.y++;

	if (mPos.y > SCR_HEIGHT) {
		game->mListToDelete.push_back(dynamic_cast<Entity*>(this));
	}
}

bool EntityBullet::NotifyCollision(CollisionManager::CollisionObject obj) {
	/*if (obj.mEntityType == EBlock) {
		game->mListToDelete.push_back(dynamic_cast<Entity*>(this));
		game->mListToDelete.push_back(dynamic_cast<Entity*>(obj.mListener));
	}*/

	return false;
}
