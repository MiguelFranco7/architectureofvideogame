#pragma once
#include <map>
#include <string>
#include "rapidjson\document.h"
#include "rapidjson/filereadstream.h"

using namespace std;

class LocationManager {
public:
						LocationManager(int languajeId);
	void				changeLanguaje(int languajeId);
	rapidjson::Document loadLanguajeJson(const char* filename);
	string				getText(string key);
	string				getLanguaje();
	int					getLanguajeId();

private:
	int					mLanguajeId;
	map<string, string> mMapLanguaje;
};