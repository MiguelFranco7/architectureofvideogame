#include "stdafx.h"
#include "EntityLiveObject.h"
#include "global.h"
#include "Game.h"

void EntityLiveObject::run() {
	mPos.y--;

	if (mPos.y < 0) {
		game->mListToDelete.push_back(dynamic_cast<Entity*>(this));
	}
}

bool EntityLiveObject::NotifyCollision(CollisionManager::CollisionObject obj) {
	/*if (obj.mEntityType == EPlayer) {
		game->mListToDelete.push_back(dynamic_cast<Entity*>(this));
		game->mLives++;
	}*/

	return false;
}
