#pragma once
#include <string>

using namespace std;

class AudioManager {
public:
	AudioManager();
	bool getMusicOn();
	void setMusicOn();
	bool getEfectsOn();
	void setEfectsOn();

	string getStringMusicOn();
	string getStringEfectsOn();

private:
	bool mMusicOn;
	bool mEfectsOn;
};