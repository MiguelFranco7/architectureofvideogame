#include "stdafx.h"
#include "EntityBulletObject.h"
#include "global.h"
#include "Game.h"

void EntityBulletObject::run() {
	mPos.y--;

	if (mPos.y < 0) {
		game->mListToDelete.push_back(dynamic_cast<Entity*>(this));
	}
}

bool EntityBulletObject::NotifyCollision(CollisionManager::CollisionObject obj) {
	/*if (obj.mEntityType == EPlayer) {
		game->mListToDelete.push_back(dynamic_cast<Entity*>(this));
		game->mCanShoot = true;
		game->mNumShots = 10;
	}*/

	return false;
}
