#pragma once

class Game;
class ApplicationGestor;
class InputManager;
class CollisionManager;
class GraphicsEngine;
class LocationManager;
class AudioManager;

extern Game				 *game;
extern ApplicationGestor *gestorApp;
extern InputManager		 *inputManager;
extern CollisionManager  *collisionManager;
extern GraphicsEngine	 *graphicsEngine;
extern LocationManager   *locationManager;
extern AudioManager		 *audioManager;
