#pragma once
#include "IApplicationMode.h"

class ApplicationModeMainMenu : public IApplicationMode {
public:
	void			  init();
	void			  load();
	void			  run();
	void			  processInput();
	void			  render();
	IdApplicationMode getIdApplicationMode();
	void			  desactivate();
	void			  unLoad();
	void			  destroy();
};