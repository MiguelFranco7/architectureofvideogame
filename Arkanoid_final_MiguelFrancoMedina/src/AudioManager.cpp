#include "stdafx.h"
#include "AudioManager.h"

AudioManager::AudioManager() { mMusicOn = true; mEfectsOn = true; }

bool AudioManager::getMusicOn()  { return mMusicOn; }
void AudioManager::setMusicOn()  { mMusicOn = !mMusicOn; }
bool AudioManager::getEfectsOn() { return mEfectsOn; }
void AudioManager::setEfectsOn() { mEfectsOn = !mEfectsOn; }

string AudioManager::getStringMusicOn() {
	if (mMusicOn)
		return "ON";
	else
		return "OFF";
}

string AudioManager::getStringEfectsOn() {
	if (mEfectsOn)
		return "ON";
	else
		return "OFF";
}
