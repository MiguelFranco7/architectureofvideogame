#include "stdafx.h"
#include "EntityBall.h"
#include "sys.h"
#include "global.h"
#include "Game.h"

void EntityBall::run() {
	mOldPos = mPos;
	vec2 newpos = vadd(mOldPos, mVel);

	bool collision = false;

	mPos = newpos;
	// Rebound on margins
	if (mVel.x > 0.0) {
		if (mPos.x > SCR_WIDTH)
			mVel.x *= -1.0;
	} else {
		if (mPos.x < 0)
			mVel.x *= -1.0;
	}
	if (mVel.y > 0.0) {
		if (mPos.y > SCR_HEIGHT)
			mVel.y *= -1.0;
	} else {
		if (mPos.y < 0) {
			mVel.y *= -1.0;
			game->mLives--;
		}
	}
}

bool EntityBall::NotifyCollision(CollisionManager::CollisionObject obj) {
	//if (obj.mEntityType == EPlayer) {
	//	// Rebound with player.
	//		if (mPos.x <= obj.mPos->x + obj.mSize->x / 2 - 10)
	//			mVel.x = -5.f;
	//		else if (mPos.x <= obj.mPos->x + obj.mSize->x / 2 + 10 && mPos.x >= obj.mPos->x + obj.mSize->x / 2 - 10)
	//			mVel.x = 0.f;
	//		else if (mPos.x >= obj.mPos->x + obj.mSize->x / 2 + 10)
	//			mVel.x = 5.f;

	//		mVel.y *= -1.f;
	//		mPos = vadd(mOldPos, mVel);
	//} else if (obj.mEntityType == EBlock) {
	//	game->mListToDelete.push_back(dynamic_cast<Entity*>(obj.mListener));

	//	mVel.y *= -1.f;
	//	mPos = vadd(mOldPos, mVel);

	//	game->mNumBlocks--;
	//}

	return false;
}
