#include "stdafx.h"
#include "GraphicsEngine.h"

GraphicsEngine::~GraphicsEngine() {
	mListSprite.~vector();
}

void GraphicsEngine::render() {
	for (size_t i = 0; i < mListSprite.size(); i++)
		CORE_RenderCenteredSprite(vmake((*mListSprite[i]).mPos->x + (*mListSprite[i]).mSize->x / 2, (*mListSprite[i]).mPos->y), vmake((*mListSprite[i]).mSize->x, (*mListSprite[i]).mSize->y), *(*mListSprite[i]).mTex);
}

GraphicsEngine::Sprite::Sprite(vec2 * pos, vec2 * size, GLuint * tex) {
	mPos  = pos;
	mSize = size;
	mTex  = tex;
}
