#pragma once
#include "IApplicationMode.h"
#include "InputManager.h"
#include "GraphicsEngine.h"
#include "rapidjson\document.h"
#include "rapidjson/filereadstream.h"
#include "applicationModePauseMenu.h"

class ApplicationModeGame : public IApplicationMode {
public:
	ApplicationModeGame(int idLevel);
	~ApplicationModeGame();
	
	void			  init();
	void			  load();
	void			  run();
	void			  processInput();
	void			  render();
	IdApplicationMode getIdApplicationMode();
	void			  desactivate();
	void			  unLoad();
	void			  destroy();

private:
	rapidjson::Document loadLevel(const char* filename);
	int					 mIdLevel;
	IApplicationMode    *mPauseMode;
};
