#pragma once
#include "Entity.h"
#include "CollisionManager.h"

class EntityBall : public Entity, public CollisionManager::CollisionListener {
public:
	vec2					mPos;
	vec2					mVel;
	vec2					mSize;
	GLuint					mGfx;
	float					mRadius;
	GraphicsEngine::Sprite *mSprite;

	EntityBall(vec2 pos, vec2 vel, GLuint gfx, float radius) {
		mPos    = pos;
		mVel    = vel;
		mSize   = vmake(radius * 2.f, radius * 2.f);
		mGfx    = gfx;
		mRadius = radius;
	}
	void run();
	virtual bool NotifyCollision(CollisionManager::CollisionObject obj);

private:
	vec2 mOldPos;
};
