#pragma once
#include "Entity.h"

class EntityBlock : public Entity, public CollisionManager::CollisionListener {
public:
	vec2       mPos;
	vec2       mVel;
	GLuint     mGfx;
	vec2	   mSize;
	GraphicsEngine::Sprite *mSprite;

	EntityBlock(vec2 pos, vec2 vel, GLuint gfx, vec2 size) {
		mPos = pos;
		mVel = vel;
		mGfx = gfx;
		mSize = size;
	}
	void run() {}
	virtual bool NotifyCollision(CollisionManager::CollisionObject obj) {
		int i = 0;
		i++;
		return true;
	}
};