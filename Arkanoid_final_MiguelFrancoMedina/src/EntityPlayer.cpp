#include "stdafx.h"
#include "EntityPlayer.h"

void EntityPlayer::run() {
	if (mUp)
		mPos = vmake(mPos.x, mPos.y + 7);

	if (mDown)
		mPos = vmake(mPos.x, mPos.y - 7);

	if (mLeft)
		mPos = vmake(mPos.x - 7, mPos.y);

	if (mRight)
		mPos = vmake(mPos.x + 7, mPos.y);
}

void EntityPlayer::setInput(bool right, bool left, bool up, bool down) {
	mRight = right;
	mLeft  = left;
	mUp	   = up;
	mDown  = down;
}
