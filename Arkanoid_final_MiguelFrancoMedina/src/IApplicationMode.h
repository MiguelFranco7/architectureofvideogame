#pragma once

enum IdApplicationMode {
	MA_NULL,
	MA_MENU,
	MA_GAME,
	MA_MAIN_MENU,
	MA_OPTIONS_MENU,
	MA_PAUSE_MENU
};

class IApplicationMode {
public:
	virtual void init()								 = 0;
	virtual void load()								 = 0;
	virtual void run()								 = 0;
	virtual void processInput()						 = 0;
	virtual void render()							 = 0;
	virtual IdApplicationMode getIdApplicationMode() = 0;
	virtual void desactivate()						 = 0;
	virtual void unLoad()							 = 0;
	virtual void destroy()							 = 0;
};