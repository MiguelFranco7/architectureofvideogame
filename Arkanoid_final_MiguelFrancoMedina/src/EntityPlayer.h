#pragma once
#include "Entity.h"
#include "CollisionManager.h"

class EntityPlayer : public Entity, public CollisionManager::CollisionListener {
public:
	vec2					mPos;
	vec2					mSpeed;
	GLuint					mGfx;
	vec2					mSize;
	bool					mBigSize;
	bool					mRight;
	bool					mLeft;
	bool					mUp;
	bool					mDown;
	GraphicsEngine::Sprite *mSprite;

	EntityPlayer(vec2 pos, vec2 speed, GLuint gfx, vec2 size) {
		mPos	  = pos;
		mSpeed	  = speed;
		mGfx	  = gfx;
		mSize	  = size;
		mBigSize  = false;
	}

	void run();
	void setInput(bool right, bool left, bool up, bool down);

	virtual bool NotifyCollision(CollisionManager::CollisionObject obj) {
		/*int i = 0;
		i++;
		if (obj.mEntityType == EBlock)
			return true;
		else
			return false;*/
	}
};
