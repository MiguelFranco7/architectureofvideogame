#pragma once
#include "IApplicationMode.h"

class ApplicationModeMenu : public IApplicationMode {
public:
	void			  init();
	void			  load();
	void			  run();
	void			  processInput();
	void			  render();
	IdApplicationMode getIdApplicationMode();
	void			  desactivate();
	void			  unLoad();
	void			  destroy();
};
