#include "stdafx.h"
#include "applicationModeMenu.h"
#include "applicationGestor.h"
#include "global.h"
#include "core.h"
#include "sys.h"
#include "font.h"
#include "LocationManager.h"

void ApplicationModeMenu::init() {

}

void ApplicationModeMenu::load() {
	FONT_Init();
	
	//texMenu = CORE_LoadPNG("../data/background_brick.png", true);
}

void ApplicationModeMenu::run() {

}

void ApplicationModeMenu::processInput() {
	if (SYS_KeyPressed(SYS_KEY_1)) {
		gestorApp->setDesiredMode(MA_GAME);
		gestorApp->setIdLevelSelected(0);
	}

	if (SYS_KeyPressed(SYS_KEY_2)) {
		gestorApp->setDesiredMode(MA_GAME);
		gestorApp->setIdLevelSelected(1);
	}

	if (SYS_KeyPressed(SYS_KEY_3)) {
		gestorApp->setDesiredMode(MA_GAME);
		gestorApp->setIdLevelSelected(2);
	}

	if (SYS_KeyPressed(SYS_KEY_4)) {
		gestorApp->setDesiredMode(MA_MAIN_MENU);
	}
}

void ApplicationModeMenu::render() {
	// Render clear
	glClear(GL_COLOR_BUFFER_BIT);

	// Text
	string title  = locationManager->getText("SG_Title");
	string easy   = locationManager->getText("SG_Easy");
	string medium = locationManager->getText("SG_Medium");
	string hard	  = locationManager->getText("SG_Hard");
	string back   = locationManager->getText("SG_Back");

	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 200), title.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 100), easy.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 + 30), medium.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 - 40), hard.c_str());
	FONT_DrawString(vmake(SCR_WIDTH / 2 - 200, SCR_HEIGHT / 2 - 110), back.c_str());

	SYS_Show();
}

IdApplicationMode ApplicationModeMenu::getIdApplicationMode() { return MA_MENU; }

void ApplicationModeMenu::desactivate() {
	// TODO
}

void ApplicationModeMenu::unLoad() {
	FONT_End();
}

void ApplicationModeMenu::destroy() {
	// TODO
}
