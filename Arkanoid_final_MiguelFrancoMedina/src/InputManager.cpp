#include "stdafx.h"
#include "InputManager.h"
#include "Component.h"
#include <algorithm>

void InputManager::processInput() {
	bool right = false;
	bool left  = false;
	bool up    = false;
	bool down  = false;
	bool space = false;

	if (SYS_KeyPressed(SYS_KEY_UP))
		up = true;

	if (SYS_KeyPressed(SYS_KEY_DOWN))
		down = true;

	if (SYS_KeyPressed(SYS_KEY_LEFT))
		left = true;

	if (SYS_KeyPressed(SYS_KEY_RIGHT))
		right = true;

	if (SYS_KeyPressed(SYS_KEY_TAB))
		game->SetpauseGame(true);

	if (SYS_KeyPressed(SYS_KEY_SPACE)) {
		space = true;
		notifyShootComponents();
	}

	if (SYS_KeyPressed(SYS_MB_LEFT))
		notifyShootComponents();

	notifyMovementComponents(right, left, up, down);
}

void InputManager::insertMovementComponent(Component * component) {
	listMovement.push_back(component);
	
	sort(listMovement.begin(),
		listMovement.end(),
		[](const Component *lhs, const Component *rhs) {
		return lhs->mPriority< rhs->mPriority;
		});
}

void InputManager::deleteMovementComponent(Component * component) {	
	for (auto it = listMovement.begin(); it != listMovement.end(); it++) {
		if (*it == component)
			listMovement.erase(it);
	}
}

void InputManager::insertShootComponent(Component * component) {
	listShoot.push_back(component);

	sort(listShoot.begin(),
		listShoot.end(),
		[](const Component *lhs, const Component *rhs) {
		return lhs->mPriority< rhs->mPriority;
	});
}

void InputManager::deleteShootComponent(Component * component) {
	for (auto it = listShoot.begin(); it != listShoot.end(); it++) {
		if (*it == component)
			listShoot.erase(it);
	}
}

void InputManager::insertOption1(Component * component) {
	listOption1.push_back(component);

	sort(listOption1.begin(),
		listOption1.end(),
		[](const Component *lhs, const Component *rhs) {
		return lhs->mPriority< rhs->mPriority;
	});
}

void InputManager::deleteOption1(Component * component) {
	for (auto it = listOption1.begin(); it != listOption1.end(); it++) {
		if (*it == component)
			listOption1.erase(it);
	}
}

void InputManager::insertOption2(Component * component) {
	listOption2.push_back(component);

	sort(listOption2.begin(),
		listOption2.end(),
		[](const Component *lhs, const Component *rhs) {
		return lhs->mPriority< rhs->mPriority;
	});
}

void InputManager::deleteOption2(Component * component) {
	for (auto it = listOption2.begin(); it != listOption2.end(); it++) {
		if (*it == component)
			listOption2.erase(it);
	}
}

void InputManager::insertOption3(Component * component) {
	listOption3.push_back(component);

	sort(listOption3.begin(),
		listOption3.end(),
		[](const Component *lhs, const Component *rhs) {
		return lhs->mPriority< rhs->mPriority;
	});
}

void InputManager::deleteOption3(Component * component) {
	for (auto it = listOption3.begin(); it != listOption3.end(); it++) {
		if (*it == component)
			listOption3.erase(it);
	}
}

void InputManager::insertOption4(Component * component) {
	listOption4.push_back(component);

	sort(listOption4.begin(),
		listOption4.end(),
		[](const Component *lhs, const Component *rhs) {
		return lhs->mPriority< rhs->mPriority;
	});
}

void InputManager::deleteOption4(Component * component) {
	for (auto it = listOption4.begin(); it != listOption4.end(); it++) {
		if (*it == component)
			listOption4.erase(it);
	}
}

void InputManager::notifyMovementComponents(bool r, bool l, bool u, bool d) {
	for (int i = 0; i < listMovement.size(); i++) {
		if (listMovement[i]->ReceiveMessage(new MovePlayerMessage(r, l, u, d)))
			break;
	}
}

void InputManager::notifyShootComponents() {
	for (int i = 0; i < listShoot.size(); i++) {
		if (listShoot[i]->ReceiveMessage(new ShootMessage()))
			break;
	}
}

void InputManager::notifyOption1Components() {
	for (int i = 0; i < listOption1.size(); i++) {
		if (listOption1[i]->ReceiveMessage(new ShootMessage()))
			break;
	}
}

void InputManager::notifyOption2Components() {
	for (int i = 0; i < listOption2.size(); i++) {
		if (listOption2[i]->ReceiveMessage(new ShootMessage()))
			break;
	}
}

void InputManager::notifyOption3Components() {
	for (int i = 0; i < listOption3.size(); i++) {
		if (listOption3[i]->ReceiveMessage(new ShootMessage()))
			break;
	}
}

void InputManager::notifyOption4Components() {
	for (int i = 0; i < listOption4.size(); i++) {
		if (listOption4[i]->ReceiveMessage(new ShootMessage()))
			break;
	}
}
